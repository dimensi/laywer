module.exports = {
	app: './app',
	public: './public',

	pugs: './app/pages/*.pug',
	stylus: './app/styles/app.styl',
	assets: './app/assets/**/*',

	vendors: './app/vendors'
};