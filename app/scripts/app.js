import 'magnific.js';
import 'search-form/search-form';
import 'mobile-button/mobile-button';
import 'm-menu/m-menu.js';
import 'formatter.js/dist/jquery.formatter';
import 'form/form';

$(document).ready(function () {
	$('.callback-button, .index__call-us').magnificPopup({
		items: [{
			src: '#modal-form',
			type: 'inline'
		}],

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});

	$('.form__input[type=tel]').formatter({
		'pattern': '+7 ({{999}}) {{999}}-{{99}}-{{99}}'
	});

});

exports.$ = $;