$('.m-menu__item--have_content').on('click', function() {
	const $this = $(this);
	if ($this.hasClass('m-menu__item--have_content--open')) {
		$this.removeClass('m-menu__item--have_content--open');
		$this.children('.m-menu__list').slideUp(300);
		return;
	}
	$('.m-menu__item--have_content').removeClass('m-menu__item--have_content--open');
	$('.m-menu__item--have_content > .m-menu__list').slideUp(300);
	$this.addClass('m-menu__item--have_content--open');
	$this.children('.m-menu__list').slideDown(300);
});