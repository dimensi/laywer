$('.search-form__button').on('click', function () {
	$(this).parent().find('.search-form__form-wrapper').addClass('search-form__form-wrapper--active');
	$(document).on('mouseup', searchClose);
});



function searchClose (e) {
	var container = $('.search-form__form-wrapper');
	if (container.hasClass('search-form__form-wrapper--active') && !container.is(e.target) 
	&& container.has(e.target).length === 0) {
		container.removeClass('search-form__form-wrapper--active');
		$(document).off('mouseup', searchClose);
	}
}