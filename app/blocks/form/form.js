import FormValidator from 'validate-js';

$(document).ready(function () {
	window.afValidated = false;
	const validator = new FormValidator('callback-form', [{
		name: 'name',
		display: 'ФИО',
		rules: 'required|min_length[3]|max_length[30]'
	}, {
		name: 'phone',
		display: 'телефон',
		rules: 'required|callback_check_phone'
	}], function (errors) {
		if (errors.length > 0) {
			$('.form__errors').empty().hide();
			for (let err of errors) {
				err.messages.forEach(function(el) {
					$('.form__errors').append(`${el} <br>`).fadeIn(300);
				});
			}
		} else {
			window.afValidated = true;
		}
	});

	validator.registerCallback('check_phone', function (value) {
		if (value.length === 18) {
			return true;
		}
		return false;
	});

	validator.setMessage('required', 'Поле <b>%s</b> должно быть заполненно');
	validator.setMessage('min_length', 'Поле <b>%s</b> должно содержать минимум <b>%s</b> символов');
	validator.setMessage('max_length', 'Поле <b>%s</b> не должно превышать <b>%s</b> символов');
	validator.setMessage('check_phone', 'Поле <b>%s</b> должно быть не меньше 10 цифр');
});